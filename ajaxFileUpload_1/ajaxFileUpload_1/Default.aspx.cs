﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace ajaxFileUpload_1
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void fileUpload_Click(object sender, EventArgs e)
        {
            string strName = FileUpload1.PostedFile.FileName;//使用fileupload控件获取上传文件的文件名
            if (strName != "")//如果文件名存在
            {
                bool fileOK = false;
                int i = strName.LastIndexOf(".");//获取。的索引顺序号，在这里。代表图片名字与后缀的间隔
                string kzm = strName.Substring(i);//获取文件扩展名的另一种方法 string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                string newName = Guid.NewGuid().ToString();//生成新的文件名，保证唯一性

                string xiangdui = @"~\images\";//设置文件相对网站根目录的保存路径 ，~号表示当前目录，在此表示根目录下的images文件夹
                string juedui = Server.MapPath("~\\images\\");//设置文件保存的本地目录绝对路径，对于路径中的字符“＼”在字符串中必须以“＼＼”表示，因为“＼”为特殊字符。或者可以使用上一行的给路径前面加上＠
                string newFileName = juedui + newName + kzm;
                if (FileUpload1.HasFile)//验证 FileUpload 控件确实包含文件
                {

                    String[] allowedExtensions = { ".gif", ".png", ".bmp", ".jpg", ".txt",".avi" };
                    for (int j = 0; j < allowedExtensions.Length; j++)
                    {
                        if (kzm == allowedExtensions[j])
                        {
                            fileOK = true;
                        }
                    }
                }
                if (fileOK)
                {
                    try
                    {
                        // 判定该路径是否存在
                        if (!Directory.Exists(juedui))
                            Directory.CreateDirectory(juedui);
                        Label1.Text = newFileName;     //为了能看清楚我们提取出来的图片地址，在这使用label
                        Label2.Text = "<b>原文件路径：</b>" + FileUpload1.PostedFile.FileName + "<br />" +
                                          "<b>文件大小：</b>" + FileUpload1.PostedFile.ContentLength + "字节<br />" +
                                          "<b>文件类型：</b>" + FileUpload1.PostedFile.ContentType + "<br />";
                        Label3.Text = xiangdui + newName + kzm;
                        Label4.Text = "文件上传成功.";
                        FileUpload1.PostedFile.SaveAs(newFileName);//将图片存储到服务器上
                    }
                    catch (Exception)
                    {
                        Label4.Text = "文件上传失败.";
                    }
                }
                else
                {
                    Label4.Text = "只能够上传图片文件.";
                }
            }
        }
    }
}